#!/usr/bin/python3

import argparse
import os
import requests
import sys
from requests.auth import HTTPBasicAuth

# Wordpress user info and url from environment variables
wp_user = os.getenv("WP_USER")
wp_passwd = os.getenv("WP_PASSWORD")
wp_url = os.getenv("WP_URL")


def check_vars():
    if wp_user is None or len(wp_user) == 0:
        raise ValueError(
            "Wordpress user name must not be None or zero length.")
    elif wp_passwd is None or len(wp_passwd) == 0:
        raise ValueError(
            "Wordpress user password must not be None or zero length.")
    elif wp_url is None or len(wp_url) == 0:
        raise ValueError("Wordpress URL must not be None or zero length.")
    return


def get_latest():
    # check environment variables
    check_vars()
    # return most recently posted blog entry
    r = requests.get(wp_url, auth=HTTPBasicAuth(wp_user, wp_passwd))
    blog_posts = r.json()
    return blog_posts[0]["title"], blog_posts[0]["content"], r.status_code


def upload_post(blog_file):
    # check environment variables
    check_vars()
    # upload blog entry stored in a file
    blog_title = ""
    blog_body = ""
    with open(blog_file) as post:
        for line in post:
            if len(blog_title) == 0 and len(line) != 0:
                blog_title = line
            else:
                blog_body += line
    print("blog title = ", blog_title)
    print("blog body = ", blog_body)
    blog_post = {
        "title": blog_title,
        "content": blog_body,
        "excerpt": blog_title,
        "status": "publish",
    }
    r = requests.post(
        wp_url,
        auth=HTTPBasicAuth(
            wp_user,
            wp_passwd),
        data=blog_post)
    return r.status_code


def upload_stdin(user_input):
    # check environment variables
    check_vars()
    # upload blog entry input to stdin
    blog_title = ""
    blog_body = ""
    for line in user_input:
        if len(blog_title) == 0 and len(line) != 0:
            blog_title = line
        else:
            blog_body += line
    print("blog title = ", blog_title)
    print("blog body = ", blog_body)
    blog_post = {
        "title": blog_title,
        "content": blog_body,
        "excerpt": blog_title,
        "status": "publish",
    }
    r = requests.post(
        wp_url,
        auth=HTTPBasicAuth(
            wp_user,
            wp_passwd),
        data=blog_post)
    return r.status_code


if __name__ == "__main__":
    # parse cli command
    parser = argparse.ArgumentParser()
    parser.add_argument(dest="action", type=str, help="read or upload")
    parser.add_argument("-f", type=str, help="filename or - for stdin")
    args = parser.parse_args()
    if args.action == "read":
        title, content, status = get_latest()
        print(title)
        print(content)
        print(status)
    elif args.action == "upload":
        if args.f == "-":
            from_stdin = sys.stdin.readlines()
            status_code = upload_stdin(from_stdin)
        elif args.f is not None:
            # check args.f is a valid file
            if os.path.exists(args.f):
                status_code = upload_post(args.f)
            else:
                print("File specified does not exist.")
        else:
            print("input file was not specified.")
    else:
        print("read/upload not specified.")
