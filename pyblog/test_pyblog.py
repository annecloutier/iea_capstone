#!/usr/bin/python3
"""This document represents the unit tests
for the Load Runner team Pyblog site
We test our 3 functions of our pyblog.py code
get_latest() is tested by test_request_get()
upload_post() is tested by test_upload_post()
upload_stdin() is tested by test_upload_stdin()
"""
from unittest import mock
import unittest
import pyblog


class MockResponse:
    """Creates a class for a MockResponse
    Object. Has the attributes of
    text and a status code
    """

    def __init__(self, text, status_code):
        """ Initializer for class
        """
        self.text = text
        self.status_code = status_code

    def json(self):
        """ Returns the text
        """
        return self.text

    def raise_for_status(self):
        """ returns the status code
        """
        return self.status_code


def mock_get_location(url, auth):
    """Mocks the request.get function used in pyblog
    rather than reach out to staging/prod site
    this module will return mock data
    with a 200 status code
    a title of "My Title" and content "My Content"
    """
    print(url)
    print(auth)
    if url and auth:
        status_code = 200
        text = [{"title": "My Title", "content": "My Content"}]
    else:
        status_code = 400
        text = [{"title": "My Title (failure case)", "content": "My Content"}]
    response = MockResponse(text, status_code)
    return response


def mock_requests_post(url, auth, data):
    """Mocks the POST to our pyblog site
    """
    if url and auth and data:
        status_code = 200
        text = ["Upload Success"]
    else:
        status_code = 400
        text = ["Upload Fail"]
    return MockResponse(text, status_code)


class PyblogTests(unittest.TestCase):
    """Unit tests for PyBlog
    """
    # pylint: disable=R0201
    @mock.patch('requests.get', new=mock_get_location)
    def test_request_get(self):
        """ Unit test for the get_location() function in pyblog
        """
        pyblog.wp_url = "www.google.com"
        pyblog.wp_passwd = "password"
        pyblog.wp_user = "request_get_user"
        title, data, status_code = pyblog.get_latest()
        assert status_code == 200
        assert title == 'My Title'
        assert data == 'My Content'

    @mock.patch('requests.post', new=mock_requests_post)
    def test_upload_post(self):
        """ unit test for uploading a post to pyblog site via a file
        """
        pyblog.wp_url = "www.google.com"
        pyblog.wp_passwd = "password"
        pyblog.wp_user = "upload_file_user"
        file_data = ("My Upload Title")
        mock_open = mock.mock_open(read_data=file_data)
        with mock.patch("builtins.open", mock_open):
            status_code = pyblog.upload_post('filename')
        assert status_code == 200

    @mock.patch('requests.post', new=mock_requests_post)
    def test_upload_stdin(self):
        """ unit test for uploading post to pyblog via standard input
        """
        pyblog.wp_url = "www.google.com"
        pyblog.wp_passwd = "password"
        pyblog.wp_user = "upload_stdin_user"
        user_input = "My Title from standard input\nMy Content from Standard input"
        status_code = pyblog.upload_stdin(user_input)
        assert status_code == 200

    def test_blank_url(self):
        """Testing for error handling for blank URL
        """
        pyblog.wp_user = "bryan"
        pyblog.wp_url = None
        pyblog.wp_passwd = "password"
        with self.assertRaises(Exception) as url_is_none:
            pyblog.get_latest()
        err = url_is_none.exception
        # print(err)
        self.assertEqual(
            str(err),
            "Wordpress URL must not be None or zero length.")

    def test_blank_user(self):
        """Testing for error handling for blank user
        """
        pyblog.wp_user = None
        pyblog.wp_url = "https://www.google.com"
        pyblog.wp_passwd = "password"
        with self.assertRaises(Exception) as user_is_none:
            pyblog.get_latest()
        err = user_is_none.exception
        # print(err)
        self.assertEqual(
            str(err),
            "Wordpress user name must not be None or zero length.")

    def test_password(self):
        """Tesing for error handling for blank passwd
        """
        pyblog.wp_user = "bryan"
        pyblog.wp_url = "https://www.google.com"
        pyblog.wp_passwd = None
        with self.assertRaises(Exception) as passwd_is_none:
            pyblog.get_latest()
        err = passwd_is_none.exception
        # print(err)
        self.assertEqual(
            str(err),
            "Wordpress user password must not be None or zero length.")


if __name__ == '__main__':
    unittest.main()
